//Drew McDermott
//adm75

//ID3 tag

#include <stdio.h>
#include <stdlib.h>



//ID3 struct

struct ID3
{
	char song[30];
	char art[30];
	char alb[30];
	char year[4];
	char comm[29];  //29 so it will include the 0 field.
	char trk[1];
	char gen[1];

};

int openID3(char fname[], struct ID3 * tag);
int printID3(struct ID3 * tag);
int editID3(struct ID3 * tag, char field[], char value[]);
int writeID3(char fname[], struct ID3 * tag);


int main(int argc, char* argv[])
{

	
	if(argc >= 2) 
	{
		struct ID3 tag;
		
		if(openID3(argv[1], &tag))
		{
			//make tag
			memset(&tag, 0, 125);
			
		}
		
		
		if(argc >= 4) //Edit tag
		{
			if(argc % 2 == 0)
			{
				int i;
				
				for(i = 2; i < argc; i += 2)
				{
					if(editID3(&tag, argv[i], argv[i+1]))
					{
						printf("\nField \"%s\" is not valid\n",argv[i]);
					
					}
				
				}
			
			
			}
			else
			{
				printf("\nSyntax Error");
				printf("\nid3tagEd filename [-(field title) (field value)]\n");
				return 1;
			
			}
			
			//Save tag
			writeID3(argv[1], &tag);
		
		}

		printID3(&tag);
		
		
		
		
	}
	
	return 0;

}

int openID3(char fname[], struct ID3 * tag)
{
	char mode[4] = "rb";
	

	
	FILE* f = fopen(fname, mode);
	
	if(f == NULL)
	{
		return 1; //Failure - FOpen error
	}
	

	
	fseek(f, -128, SEEK_END); //Go to start of tag
	
	char buffer[32] = {0};
	
	fread(buffer, 3, 1, f);
	
	if(strcmp(buffer, "TAG"))
	{
		//printf("Tag Not Found\n%s\n", buffer);
		return 2; //Failure - TAG not found
	}
	

	
	fread(tag,1,125,f);
	fclose(f);
	
	return 0;
	

	
}

int printID3(struct ID3 * tag)
{
	printf("\nTag Contents:");
	printf("\n\nSong: %.30s", (*tag).song);
	printf("\nArtist: %.30s", (*tag).art);
	printf("\nAlbum: %.30s", (*tag).alb);
	printf("\nYear: %.4s", (*tag).year);
	printf("\nComment: %s", (*tag).comm);
	printf("\nTrack: %d", (int)((*tag).trk[0]));
	printf("\nGenre: %.1s", (*tag).gen);
	printf("\n");
}

int editID3(struct ID3 * tag, char field[], char value[])
{
	if(!strcmp(field,"-song"))
	{
		strncpy((*tag).song, value, 30);
	}
	else if(!strcmp(field,"-artist"))
	{
		strncpy((*tag).art, value, 30);
	}
	else if(!strcmp(field,"-album"))
	{
		strncpy((*tag).alb, value, 30);
	}
	else if(!strcmp(field,"-year"))
	{
		strncpy((*tag).year, value, 4);
	}
	else if(!strcmp(field,"-comment"))
	{
		strncpy((*tag).comm, value, 28);
	}
	else if(!strcmp(field,"-track"))
	{
		(*tag).trk[0] = (char)atoi(value);
	}
	else if(!strcmp(field,"-genre"))
	{
		(*tag).gen[0] = value[0];
	}
	else
	{
		return 1; //Field not identified	
	}
	
	return 0;

}

int writeID3(char fname[], struct ID3 * tag)
{
	char mode[4] = "ab";

	FILE* f = fopen(fname, mode);
	
	if(f == NULL)
	{
		return 1; //Failure - FOpen error
	}

	fseek(f, -128, SEEK_END); //Go to start of tag
	
	char buffer[32] = "TAG";
	
	fwrite(buffer, 3, 1, f);
	fwrite(tag,1,125,f);
	fclose(f);
	
	return 0;
	

	
}
